# HomeAssistant & Home Automation



Home Assistant is running on an Odroid N2, N2+ and C4.



## Where to start

- [Home-Assistant.io](https://home-assistant.io/) 
- [Home-Assistant - Getting Started](https://www.home-assistant.io/getting-started/)
- [Splitting up the configuration.yaml](https://github.com/cbulock/home-assistant-configs)
- [Storing secrets](https://home-assistant.io/docs/configuration/secrets/)
- [Blakadder - Zigbee Device Compatibility Repository](https://zigbee.blakadder.com/zha.html)
- [Zigbee2MQTT - Zigbee Suported Device Library](https://www.zigbee2mqtt.io/supported-devices/)
- [Z-Wave JS Config DB Browser](https://devices.zwave-js.io)
- [Ikea Tradfri Release Notes](https://ww8.ikea.com/ikeahomesmart/releasenotes/releasenotes.html)


## My Devices

- Odroid N2
- Odroid N2+
- Odroid C4
- [Conbee II Zigbee USB Stick](https://phoscon.de/de/conbee2) (now flashed with [Thread firmware](https://phoscon.de/de/openthread/doc) from Phoscon)
- Sonoff Zigbee 3.0 USB Dongle Plus - ZBDongle-P
- [AVM FritzDECT 200](https://avm.de/produkte/smart-home/fritzdect-200/) Wall Plugs
- Google Chromecast Audio
- Google Chromecast
- [Hichi](https://sites.google.com/view/hichi-lesekopf) - Stromzähler Lesekopf - IR-WIFI (Tasmota Integration)
- Osram Smart+ Plugs - ([Z2M](https://www.zigbee2mqtt.io/devices/AB3257001NJ.html) / [Blakadder](https://zigbee.blakadder.com/OSRAM_AB3257001NJ.html))
- Sonos Loudspeaker
- IKEA TRÅDFRI - bulbs - [LED1536G5](https://www.zigbee2mqtt.io/devices/LED1536G5.html) / [LED1545G12](https://www.zigbee2mqtt.io/devices/LED1545G12.html) / [LED1924G9](https://www.zigbee2mqtt.io/devices/LED1924G9.html) / [LED1624G9](https://www.zigbee2mqtt.io/devices/LED1624G9.html) / [LED1923R5](https://www.zigbee2mqtt.io/devices/LED1923R5.html)
- IKEA TRÅDFRI - Power Plugs - E1603 - ([Z2M](https://www.zigbee2mqtt.io/devices/E1603_E1702_E1708.html) / [Blakadder](https://zigbee.blakadder.com/Ikea_E1603.html))
- IKEA TRÅDFRI  - buttons - [E1524](https://www.zigbee2mqtt.io/devices/E1524_E1810.html) / [E1810](https://zigbee.blakadder.com/Ikea_E1810.html) / [E2002](https://www.zigbee2mqtt.io/devices/E2001_E2002.html) / [E1812](https://www.zigbee2mqtt.io/devices/E1812.html)
- IKEA TRÅDFRI - Motion Sensor - E1745 - ([Z2M](https://www.zigbee2mqtt.io/devices/E1525_E1745.html) / [Blakadder](https://zigbee.blakadder.com/Ikea_E1745.html) )
- IKEA TRÅDFRI - Motion Sensor - E2134 - ([Z2M](https://www.zigbee2mqtt.io/devices/E2134.html) )
- IKEA TRÅDFRI - STARKVIND - E2007 - ([Z2M](https://www.zigbee2mqtt.io/devices/E2007.html))
- IKEA TRÅDFRI - ÅSKVÄDER switchable power plugs - E1836 - ([Z2M](https://www.zigbee2mqtt.io/devices/E1836.html))
- IKEA TRÅDFRI - PARASOLL door/window sensor - E2013 - ([Z2M](https://www.zigbee2mqtt.io/devices/E2013.html))
- IKEA TRÅDFRI - BADRING water leakage sensor - E2202 - ([Z2M](https://www.zigbee2mqtt.io/devices/E2202.html))
- Sengled RGB-LED-bulb, 800lm (via WiFi-Matter)
- Sonoff Temperature Sensor - SNZB-02 - ([Z2M](https://www.zigbee2mqtt.io/devices/SNZB-02.html) / [Blakadder](https://zigbee.blakadder.com/Sonoff_SNZB-02.html))
- Sonoff Temperature Sensor - SNZB-02D - ([Z2M](https://www.zigbee2mqtt.io/devices/SNZB-02D.html))
- Sonoff Door Contact Sensor - SNZB-04 - ([Z2M](https://www.zigbee2mqtt.io/devices/SNZB-04.html))
- Tuya Plugs - TS011F_plug_1 - ([Z2M](https://www.zigbee2mqtt.io/devices/TS011F_plug_1.html))
- [Tuya Temperature & Humidity Sensor ZTH01](https://codeberg.org/kreisklasse/Home-Assistant/src/branch/master/Devices/Tuya_Temp_Sensor_ZTH01.md)
- Ulanzi Awtrix Clock
- Z-Wave [USB.ME](https://devices.zwave-js.io/?jumpTo=0x0115:0x0000:0x0000:0.0) Stick
- Z-Wave [Fibaro FGMS001](https://manuals.fibaro.com/de/motion-sensor/) Motion Sensor
- Z-Wave Fibaro [FGWPE/F-101](http://manuals-backend.z-wave.info/make.php?lang=en&sku=SCH_FGWPF-101&cert=ZC08-14020004) / [FGWP 102](https://manuals.fibaro.com/de/wall-plug/) Wall Plugs
- Xiaomi Mi Power Plug - ([Z2M](https://www.zigbee2mqtt.io/devices/ZNCZ04LM.html) / [Blakadder](https://zigbee.blakadder.com/Xiaomi_ZNCZ04LM.html))
- Power Meter [EMH ED300L](https://codeberg.org/kreisklasse/Home-Assistant/src/branch/master/Stromzaehler/EMH%20ED300L.md)
- Power Meter [DZG DVS7420](https://codeberg.org/kreisklasse/Home-Assistant/src/branch/master/Stromzaehler/DZG%20DVS74.md)



## My Add-Ons in use
- [HACS](https://hacs.netlify.com/) - Home Assistant Community Store
- [Adguard home](https://github.com/hassio-addons/addon-adguard-home)
- [chrony](https://github.com/hassio-addons/addon-chrony)
- [deCONZ](https://www.home-assistant.io/components/deconz/)
- [EMQX - MQTT broker](https://github.com/hassio-addons/addon-emqx)
- [File Editor](https://www.home-assistant.io/addons/configurator)
- Matter
- [Netbird.io](https://github.com/netbirdio/addon-netbird) p2p-Wireguard-VPN (presently disbaled, does not work as reliable as Tailscale)
- [Tailscale with Features](https://github.com/lmagyar/homeassistant-addon-tailscale) p2p-Wireguard-VPN
- Thread
- [Zerotier](https://github.com/hassio-addons/addon-zerotier) p2p-VPN (disabled)
- [Zigbee2MQTT](https://github.com/zigbee2mqtt/hassio-zigbee2mqtt/tree/master/zigbee2mqtt)
- [Z-wave JS UI](https://github.com/hassio-addons/addon-zwave-js-ui)




## Nice to know
- [Reinstall Snapshot on new Install](https://gist.github.com/cogneato/24d5a1a0daddec9d048048201170df8b)

>Restoring a snapshot on a fresh install of Hassio requires that you upload the snapshot to Hassio in the first place. There is no way to upload snapshots from the UI at this time so the first thing to do is install and configure an addon of your preference. Samba, Configurator, or IDE are all good choices for this.

>After uploading to the backup directory, visit the snapshot page in the UI and reload the page using the icon in the upper right of the page to make the uploaded files visible. From there you can select your snapshot, choose a full or partial install and choose restore.

>Note: Before restoring third-party addons, add their repositories to the addon store page first! These are any addons which are not in the official repo or the community addon repo. Nothing bad will happen, but those addons won't get restored. You can always add the repos and then go back and do a partial restore of the missing addons.


# Help
### Update of Home Assistant does not work
1. ssh into hassio
2. run `hassio su repair`
3. update via the UI


## Firmware Release Notes
- [Home Assistant - OS](https://github.com/home-assistant/operating-system/releases)
- [Home Assistant - Supervisor](https://github.com/home-assistant/supervisor/releases)
- [Ikea Tradfri](https://ww8.ikea.com/ikeahomesmart/releasenotes/releasenotes.html)
- [Zigbee2MQTT](https://github.com/Koenkk/zigbee2mqtt/releases)