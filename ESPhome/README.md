# ESPhome

## Links
https://esphome.io  
https://esphome.io/guides/configuration-types.html#pin-schema (Pin Schema in yaml)  
https://linuxhint.com/esp32-pinout-reference/ [[https://archive.ph/Fvx0P|archive.is Link]]  
https://linuxhint.com/esp32-pull-up-pins/ [[https://archive.ph/0NY6G|archive.is Link]]  
https://www.simon42.com/home-assistant-rohr-temperatur-ds18b20/  
https://documents.jemrf.com/esp32-temperature.html


----

## Anschluss Temperatur Sensor
**AZ-Delivery ESP32-WROOM V4 & Dallas DS18B20 Temperatur Sensor** (Bild © unbekannt, sorry)  
![azdelivery_esp32_wroom32_v4_dallas_ds18b20.webp](https://codeberg.org/kreisklasse/Home-Assistant/raw/branch/master/ESPhome/azdelivery_esp32_wroom32_v4_dallas_ds18b20.webp)

**AZ-Delivery DS18B20Temperatursensor**  
Messbereich: -55°C bis +125°C / Genauigkeit: +/- 0.5°C zwischen -10°C und +85°C  
Bus Technologie: 1-Wire  
Anschlussleitung: rot (VCC), gelb (DATA), schwarz (GND)  
Spannung: 5V  
PullUp-Widerstand: 4,7 kΩ



----

## ESPhome Code
### esp32-1.yaml

```
esphome:
  name: "esp32-1"
  friendly_name: "esp32-1"

esp32:
  board: az-delivery-devkit-v4
  framework:
    type: arduino

# Enable logging
logger:

# Enable Home Assistant API
api:
  encryption:
    key: "xr4ieSdEcc1nhoSudxE="

ota:
  password: "57828bb330"

wifi:
  ssid: !secret wifi_ssid
  password: !secret wifi_password
  power_save_mode: none
  manual_ip:
    static_ip: 192.168.178.11
    gateway: 192.168.178.1
    subnet: 255.255.255.0
    dns1: 192.168.178.1

  # Enable fallback hotspot (captive portal) in case wifi connection fails
  ap:
    ssid: "Test1 Fallback Hotspot"
    password: "8zZgJH"

captive_portal:

time:
  - platform: homeassistant
    id: homeassistant_time

dallas:
  - pin: 
      number: 14
      mode: 
        input: True
        open_drain: False
        pullup: False
        pulldown: False
    update_interval: 60s

sensor:
  - platform: wifi_signal # Reports the WiFi signal strength/RSSI in dB
    name: "WiFi Signal dB"
    id: wifi_signal_db
    update_interval: 300s
    entity_category: "diagnostic"

  - platform: copy # Reports the WiFi signal strength in %
    source_id: wifi_signal_db
    name: "WiFi Signal Percent"
    filters:
      - lambda: return min(max(2 * (x + 100.0), 0.0), 100.0);
    unit_of_measurement: "Signal %"
    entity_category: "diagnostic"

  - platform: uptime
    name: Uptime Sensor

  - platform: internal_temperature
    name: "Internal Temperature"
    unit_of_measurement: "°C"
    icon: "mdi:thermometer-plus"
    device_class: "temperature"
    state_class: "measurement"
    accuracy_decimals: 2

  - platform: dallas
    address: 0x
    name: "1"
    unit_of_measurement: "°C"
    icon: "mdi:home-thermometer-outline"
    device_class: "temperature"
    state_class: "measurement"
    accuracy_decimals: 1
  - platform: dallas
    address: 0x
    name: "2"
    unit_of_measurement: "°C"
    icon: "mdi:home-thermometer-outline"
    device_class: "temperature"
    state_class: "measurement"
    accuracy_decimals: 1
  - platform: dallas
    address: 0x
    name: "3"
    unit_of_measurement: "°C"
    icon: "mdi:home-thermometer-outline"
    device_class: "temperature"
    state_class: "measurement"
    accuracy_decimals: 1
  - platform: dallas
    address: 0x
    name: "4"
    unit_of_measurement: "°C"
    icon: "mdi:home-thermometer-outline"
    device_class: "temperature"
    state_class: "measurement"
    accuracy_decimals: 1
```
