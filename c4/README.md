## Odroid C4
- https://www.hardkernel.com/shop/odroid-c4/
- https://wiki.odroid.com/odroid-c4/odroid-c4

### AddOn used
- Advanced SSH and Terminal
- Chrony
- espHome
- File Editor
- EMQX Mosquito Broker
- Taiscale with features
- Zerotier

### Integrations
- Battery Simulator (HACS Addon)
- Electricity Maps
- espHome
- Generic Camera
- HACS
- Local IP
- MQTT
- MQTT Statestream
- RESTfull Command
- Shelly
- Solar-Log
- Solcast PV Forecast
- Sun
- Sonos
- Tasmota
- Uptime
- Wolf SmartSet Service


### Cards
- Bar-Card - https://github.com/custom-cards/bar-card
- Sonos Card