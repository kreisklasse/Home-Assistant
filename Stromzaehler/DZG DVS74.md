# DZG DVS7420

## Volkszähler
https://wiki.volkszaehler.org/hardware/channels/meters/power/edl-ehz/dzg_dvs74

## Hichi IR WIFI SML Lesekopf
- Hichi IR Wifi Sensor mit Kabel nach oben anbringen
- am Zähler PIN auf OFF setzen
- dann am Zähler INF auf ON setzen
- Hichi Console: **sensor53 d1** - Daten Log-Ausgabe starten
- Hichi Console: **sensor53 d0** - Daten Log-Ausgabe stoppen
- Tasmota Integration in Home Assistant

### Hichi Script - DVS7420.2V.G2

```
>D
>B
=>sensor53 r
>M 1
+1,3,s,16,9600,DWS742
1,77070100010800ff@1000,VerbrauchGesamt,Wh,energy_total,0
1,77070100010801ff@1000,VerbrauchHT,Wh,energy_ht,0
1,77070100010802ff@1000,VerbrauchNT,Wh,energy_nt,0
1,77070100020800ff@1000,Netzeinspeisung,kWh,energy_out,1
1,77070100100700ff@1,AktuelleLeistung,W,power_curr,1
1,7707010060320101@1,Hersteller,unknown,copany,0
1,77070100600100ff@1,Zählernummer,unknown,meter_id,0
1,=so2,1
#
```

## Links
[[https://www.dzg.de/fileadmin/dzg/content/downloads/produkte-zaehler/dvs74/dzg_dvs74_Bedienungsanleitung.pdf|DZG DVS74 Anleitung pdf]]

https://tasmota.github.io/docs/Smart-Meter-Interface/#dzg-dws74102vg2-sml-and-dzg-dvs74202-sml

https://www.schatenseite.de/2016/05/30/smart-message-language-stromzahler-auslesen/comment-page-1/

https://hessburg.de/tasmota-wifi-smartmeter-konfigurieren/

https://www.photovoltaikforum.com/thread/192440-dzg-dvs74-zeigt-nur-gesamtverbrauch-und-einspeisung-an/?postID=3222863#post3222863

[[https://tasmota-sml-parser.dicp.net/|Tasmato SML Decoder]]