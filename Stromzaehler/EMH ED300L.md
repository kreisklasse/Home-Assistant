# EMH ED300L

## Volkszähler
von [[https://wiki.volkszaehler.org/hardware/channels/meters/power/edl-ehz/emh-ed300l|volkszaehler.org]]

### Kommunikation
Der [[http://www.emh-meter.de/de/produkte/ehz-h1/|EMH eHZ-H]] kann nach EN 62056-21 über die IR-Schnittstelle ausgelesen werden.\\
Allerdings muss der Zähler erst freigeschaltet werden. Die dazu nötige PIN kann beim Messtellenbetreiber (meist der Netzbetreiber) angefragt werden.
Eingegeben wird die PIN über einen Lichttaster. Den gekennzeichneten Sensor einfach mit einer Taschenlampe anleuchten.

### Hardware
Nach erfolgreichem Freischalten sendet der Zähler alle paar Sekunden unaufgefordert ein SML-Datenpaket das mit einem IR-Schreib-Lesekopf erfasst werden kann. Den Lesekopf mit dem Kabel nach OBEN auf die Metallplatte mit den beiden Löchern setzten. Gesendet wird mit 9600baud, 8N1

## Hichi IR WIFI SML Lesekopf
- Hichi IR Wifi Sensor mit Kabel nach **oben** anbringen
- Hichi Console: **sensor53 d1** - Daten Log-Ausgabe starten
- Hichi Console: **sensor53 d0** - Daten Log-Ausgabe stoppen
- Tasmota Integration in Home Assistant


### Hichi Script

```text
>D
>B
->sensor53 r
>M 1
+1,3,s,0,9600,ED300L
1,77070100100700ff@1,Aktuelle Leistung,W,power,0
1,77070100000009ff@1000,tbn1,kWh,tbn1,2
1,77070100010801ff@1000,HT Gesamt,kWh,HTGesamt,2
1,77070100020801ff@1000,tbn4,kWh,tbn4,2
1,77070100010802ff@1000,NT Gesamt,kWh,NTGesamt,2
1,77070100020802ff@1000,tbn5,kWh,tbn5,2
1,77070100010800FF@1000,Zähler-Verbrauch,kWh,verbrauch,1  
1,77070100020800FF@1000,Zähler-Einspeisung,kWh,einspeisung,0
#
```

[[https://sites.google.com/view/hichi-lesekopf|Hichi Tasmota Lesekopf - Setup & FAQ]]

[[https://tasmota.github.io/docs/MQTT/|Hichi Tasmota Lesekopf - Setup MQTT]]

[[https://tasmota.github.io/docs/Smart-Meter-Interface/#emh-ed300l-sml|Tasmoto SML Smart Meter Interface]]

## YouTube Videos & Anleitungen
Daten aus Stromzähler auslesen und in HomeAssistant verarbeiten

YoutTube - [[https://www.youtube.com/watch?v=VuXpzKetOhc|Wifi lesekopf mit Tasmota testen - krach b]]

YouTube - [[https://www.youtube.com/watch?v=nXBaemeKxZg|SML Stromzähler lokal mit Home Assistant auslesen - Simon42]]

[[https://www.simon42.com/home-assistant-sml-stromzaehler-lokal/|Homepage - Simon42 - SML auslesen]]

✔ [[https://ottelo.jimdofree.com/stromz%C3%A4hler-auslesen-tasmota/|Homepage - Otello's Blog]] | [[https://archive.ph/X1W9v|Otello's Blog im Archive.ph]]

https://hessburg.de/tasmota-wifi-smartmeter-konfigurieren/

## Links
[[https://emh-metering.com/produkte/haushaltszaehler-smart-meter/ed300l/|EMH ED300L Datenblatt und Anleitung]]

[[https://tasmota-sml-parser.dicp.net/|Tasmato SML Decoder]]